import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Controller {

    private PageReader pageReader;
    private View vacancyView;
    private VacancyHandler vacancyHandler;

    private List<Performer> options = new ArrayList<>();

    private Map<String, String> currentDou = new HashMap<>();
    private Map<String, String> currentRabota = new HashMap<>();

    public Controller(PageReader pageReader,
                      View vacancyView,
                      VacancyHandler vacancyHandler) {
        this.pageReader = pageReader;
        this.vacancyView = vacancyView;
        this.vacancyHandler = vacancyHandler;
        init();
    }

    private void init() {
        options.add(this::end);
        options.add(this::displayAll);
        options.add(this::displayOldNew);
        options.add(this::displayCurrentNew);
    }

    private void displayAll() {
        currentDou= pageReader.getDouLinks();
        currentRabota = pageReader.getRabotaLinks();
        Map<String, String> oldDou =
                vacancyHandler.deserializeMap("dou");
        Map<String, String> oldRabota =
                vacancyHandler.deserializeMap("rabota");

        vacancyView.displayVacancy("old on dou", oldDou);
        vacancyView.displayVacancy("old on rabota", oldRabota);

        vacancyView.displayVacancy("current on dou", currentDou);
        vacancyView.displayVacancy("current on rabota", currentRabota);

        Map<String, String> newDou =
                vacancyHandler.findDiff(oldDou,
                        currentDou);

        Map<String, String> newRabota =
                vacancyHandler.findDiff(oldRabota,
                        currentRabota);

        vacancyView.displayVacancy("new on dou", newDou);
        vacancyView.displayVacancy("new on rabota", newRabota);
    }

    private void displayOldNew() {
        currentDou= pageReader.getDouLinks();
        currentRabota = pageReader.getRabotaLinks();
        Map<String, String> oldDou =
                vacancyHandler.deserializeMap("dou");
        Map<String, String> oldRabota =
                vacancyHandler.deserializeMap("rabota");

        vacancyView.displayVacancy("old on dou", oldDou);
        vacancyView.displayVacancy("old on rabota", oldRabota);

        Map<String, String> newDou =
                vacancyHandler.findDiff(oldDou,
                        currentDou);

        Map<String, String> newRabota =
                vacancyHandler.findDiff(oldRabota,
                        currentRabota);

        vacancyView.displayVacancy("new on dou", newDou);
        vacancyView.displayVacancy("new on rabota", newRabota);
    }

    private void displayCurrentNew() {
        currentDou= pageReader.getDouLinks();
        currentRabota = pageReader.getRabotaLinks();
        Map<String, String> oldDou =
                vacancyHandler.deserializeMap("dou");
        Map<String, String> oldRabota =
                vacancyHandler.deserializeMap("rabota");

        vacancyView.displayVacancy("current on dou", currentDou);
        vacancyView.displayVacancy("current on rabota", currentRabota);

        Map<String, String> newDou =
                vacancyHandler.findDiff(oldDou,
                        currentDou);

        Map<String, String> newRabota =
                vacancyHandler.findDiff(oldRabota,
                        currentRabota);

        vacancyView.displayVacancy("new on dou", newDou);
        vacancyView.displayVacancy("new on rabota", newRabota);
    }

    public void display(int choice) {
        options.get(choice).perform();
        end();
    }

    private void end() {
        vacancyHandler.serializeMap("dou", currentDou);
        vacancyHandler.serializeMap("rabota", currentRabota);
        System.exit(0);
    }

}
