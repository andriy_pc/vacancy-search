import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Controller controller = new Controller(
                new PageReader(),
                new View(),
                new VacancyHandler());
        Scanner in = new Scanner(System.in);
        System.out.println("Please select option: ");
        System.out.println("0) exit");
        System.out.println("1) observe old, current and new vacancies");
        System.out.println("2) observe old and new vacancies");
        System.out.println("3) observe current and new vacancies");

        int choice = in.nextInt();
        controller.display(choice);
    }
}
