import java.util.List;
import java.util.Map;
import java.util.Set;

public class View {
    public void displayVacancy(String message, List<String> vacancies) {
        System.out.println(message);
        int i = 0;
        for (String s : vacancies) {
            System.out.println("\t" + s);
        }
        System.out.println();
    }

    public void displayVacancy(String message,
                               Map<String, String> vacancies) {
        System.out.println(message);
        Set<Map.Entry<String, String>> entries = vacancies.entrySet();
        for(Map.Entry me : entries) {
            System.out.println("\t" + me);
        }
        System.out.println();
    }
}
