import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Properties;

public class PropertiesHandler {
    private static Properties properties;

    private PropertiesHandler() {
    }

    private void init() {
        properties = new Properties();
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        getClass().getResourceAsStream("config.properties"),
                        "Cp1251"))
        ) {
            properties.load(in);
            String driverPath = new File(App.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getPath();
            properties.put("webDriverPath", driverPath);
        } catch (IOException | URISyntaxException e) {

        }
    }

    public static Properties getProperties() {
        if (properties == null) {
            PropertiesHandler ph = new PropertiesHandler();
            ph.init();
        }
        return properties;
    }
}
