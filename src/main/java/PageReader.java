import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.opera.OperaDriver;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class PageReader {

    public PageReader() {
        System.setProperty("webdriver.opera.driver",
                "operadriver.exe");
    }

    private static Logger log = LogManager.getLogger(PageReader.class);
    private static Properties properties = PropertiesHandler.getProperties();

    public List<String> getDouVacancies() {
        List<String> vacancyTitles = new ArrayList<>();
        WebDriver driver = new OperaDriver();

        driver.get(properties.getProperty("douUrl"));
        driver.findElement(By.className("more-btn"))
                .findElement(By.tagName("a"))
                .click();
        try {
            TimeUnit.SECONDS.sleep(1);  //gives time for JS to change the page's code
        } catch (InterruptedException e) {
            log.error("InterruptedException while " +
                    "getting dou vacancies: {}", e.getMessage());
        }
        List<WebElement> elements =
                driver.findElements(By.className("vt"));

        int i = 0;
        for (WebElement we : elements) {
            vacancyTitles.add(++i + ") " + we.getText());
        }
        driver.quit();
        return vacancyTitles;
    }

    public Map<String, String> getDouLinks() {
        Map<String, String> result = new LinkedHashMap<>();
        WebDriver driver = new OperaDriver();

        driver.get(properties.getProperty("douUrl"));
        driver.findElement(By.className("more-btn"))
                .findElement(By.tagName("a"))
                .click();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            log.error("InterruptedException while " +
                            "getting dou vacancies with links: {}",
                    e.getMessage());
        }

        List<WebElement> elements = driver.findElements(By.className("vt"));
        int i = 0;
        for (WebElement we : elements) {
            if(result.containsKey(we.getText())) {
                log.error("map already contains such vacancy: {}", we.getText());
            }
            result.put(++i + ") " + we.getText(),
                    we.getAttribute("href"));
        }
	driver.quit();
        return result;
    }

    public List<String> getRabotaVacancies() {
        List<String> vacancyTitles = new ArrayList<>();
        try {
            System.err.println(properties.getProperty("rabotaUrl"));
            Document doc = Jsoup.connect(properties.getProperty("rabotaUrl")).get();

            Elements elements = doc.getElementsByClass("ga_listing");
            int i = 0;
            for (Element e : elements) {
                vacancyTitles.add(++i + ") " + e.text());
            }
        } catch (IOException e) {
            log.error("IOException while " +
                    "getting rabota vacancies: {}", e.getMessage());
        }

        return vacancyTitles;
    }

    public Map<String, String> getRabotaLinks() {
        Map<String, String> result = new LinkedHashMap<>();
        try {
            Document doc = Jsoup
                    .connect(properties.getProperty("rabotaUrl"))
                    .get();
            int i = 0;
            Elements elements = doc.getElementsByClass("ga_listing");
            for(Element e : elements) {
                if(result.containsKey(e.text())) {
                    log.error("map already contains such vacancy: {}", e.text());
                }
                result.put(++i + ") " + e.text(), e.attr("href"));
            }
        } catch (IOException e) {
            log.error("IOException while " +
                            "getting rabota vacancies with links: {}",
                    e.getMessage());
        }

        return result;
    }
}
