import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VacancyHandler {

    private static Logger log = LogManager.getLogger(VacancyHandler.class);
    private static final String PATH = "";

    public void serializeList(String site, List<String> vacancies) {
        try (ObjectOutputStream out =
                     new ObjectOutputStream(
                             new FileOutputStream(PATH + site + ".txt"))
        ) {
            out.writeObject(vacancies);
        } catch (IOException e) {
            log.error("IOException occurred while serializing list: {}", e.getMessage());
        }
    }

    public List<String> deserializeList(String site) {
        List vacancies = new ArrayList();
        try (ObjectInputStream out =
                     new ObjectInputStream(
                             new FileInputStream(PATH + site + ".txt"))
        ) {
            vacancies = (List) out.readObject();
        } catch (IOException | ClassNotFoundException e) {
            log.error("IOException occurred while deserializing list: {}", e.getMessage());
        }
        return vacancies;
    }

    public void serializeMap(String site, Map<String, String> vacancies) {
        try (ObjectOutputStream out =
                     new ObjectOutputStream(
                             new FileOutputStream(PATH + site + ".txt"))
        ) {
            out.writeObject(vacancies);
        } catch (IOException e) {
            log.error("IOException occurred while serializing map: {}", e.getMessage());
        }
    }

    public Map<String, String> deserializeMap(String site) {
        Map vacancies = new HashMap();
        try (ObjectInputStream out =
                     new ObjectInputStream(
                             new FileInputStream(PATH + site + ".txt"))
        ) {
            vacancies = (Map) out.readObject();
        } catch (IOException | ClassNotFoundException e) {
            log.error("IOException occurred while deserializing map: {}", e.getMessage());
        }
        return vacancies;
    }

    public List<String> findDiff(List<String> oldVacancies,
                                 List<String> newVacancies) {
        newVacancies.removeAll(oldVacancies);
        return newVacancies;
    }

    public Map<String, String>
    findDiff(Map<String, String> oldVacancies,
             Map<String, String> newVacancies) {
        for(String k : oldVacancies.keySet()) {
            newVacancies.remove(k);
        }
        return newVacancies;
    }
}
