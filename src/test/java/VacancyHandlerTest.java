import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VacancyHandlerTest {

    private VacancyHandler vh = new VacancyHandler();
    private List<String> firstList = new ArrayList<>(Arrays.asList("First", "Second", "Third"));
    private List<String> secondList = new ArrayList<>(
            Arrays.asList("First", "Second", "Third", "Fourth")
    );
    private Map<String, String> firstMap = new LinkedHashMap<>();
    private Map<String, String> secondMap = new LinkedHashMap<>();

    @BeforeAll
    public void initMap() {
        firstMap.put("1", "2");
        firstMap.put("2", "2");
        firstMap.put("3", "2");

        secondMap.put("1", "2");
        secondMap.put("2", "2");
        secondMap.put("3", "2");
        secondMap.put("4", "2");

    }

    @Test
    void serializeDeserializeListTest() {
        vh.serializeList("test", firstList);
        List<String> result = vh.deserializeList("test");

        assertEquals(firstList, result);
    }

    @Test
    void findDiffListTest() {
        List<String> result = vh.findDiff(firstList, secondList);

        assertEquals(1, result.size());
    }

    @Test
    void serializeDeserializeMapTest() {
        vh.serializeMap("test", firstMap);

        Map<String, String> result = vh.deserializeMap("test");

        assertEquals(firstMap, result);
    }

    @Test
    void findDiffMapTest() {
        Map<String, String> result = vh.findDiff(firstMap, secondMap);

        assertEquals(1, result.size());
        assertEquals("2", result.get("4"));
    }

}